import React, { Component } from 'react';
import { connect } from 'react-redux';

import NewModal from '../components/NewPostModal'
import NavbarContainer from './NavbarContainer';
import PostFeed from './PostFeed';

export class HomePage extends Component {
  componentDidMount = () => {
    const { history } = this.props;
    if (!localStorage.jwtToken) {
      history.push('/login');
    }
  };

  render() {
    return (
      <div>
        <NavbarContainer />
        <NewModal/>
        <PostFeed />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.authReducer
});

export default connect(mapStateToProps)(HomePage);
