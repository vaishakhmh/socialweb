import React, { Component } from 'react';
import Post from './Post';
import Loading from './Loading';

class PostList extends Component {
  state = {
    following: [],
    loading: true
  };

  componentDidMount = () => {
    const { getPosts, getFollowing, user } = this.props;
    getPosts().then(() => {
      getFollowing(user.userId).then((res) => {
        const following = (res.payload && res.payload.user) ? res.payload.user.following : [];
        this.setState({
          following,
          loading: false
        });
      });
    });
  };

  checkPageType = (
    followingList,
    onProfilePage,
    postAuthorId,
    signedInUserId
  ) => {
    if (onProfilePage) {
      const { match } = this.props;
      const userProfileId = match.params.id;
      return postAuthorId === userProfileId;
    }
    return (
      followingList.includes(postAuthorId) || postAuthorId === signedInUserId
    );
  };

  render() {
    const {
      addComment,
      deleteComment,
      deletePost,
      editComment,
      editPost,
      getUser,
      posts,
      updatePostLikes,
      onProfilePage,
      user
    } = this.props;
    const { following, loading } = this.state;

    return loading ? (
      <Loading />
    ) : (
      <div>
        {posts.map(
          post =>
            (this.checkPageType(
              following,
              onProfilePage,
              post.authorId,
              user.userId
            ) ? (
              <Post
                key={post._id}
                _id={post._id}
                author={post.author}
                authorId={post.authorId}
                avatarColor={post.avatarColor}
                comments={post.comments}
                likers={post.likers}
                likesCount={post.likesCount}
                signedInUserId={user.userId}
                text={post.text}
                timestamp={post.timestamp}
                addComment={(action, commenterId, postId, text, timestamp) =>
                  addComment(action, commenterId, postId, text, timestamp)
                }
                deleteComment={(action, commentId, postId) =>
                  deleteComment(action, commentId, postId)
                }
                deletePost={id => deletePost(id)}
                editComment={(action, commentId, postId, text) =>
                  editComment(action, commentId, postId, text)
                }
                editPost={(id, text, author) => editPost(id, text, author)}
                getUser={id => getUser(id)}
                updatePostLikes={(action, postId, likerId) =>
                  updatePostLikes(action, postId, likerId)
                }
              />
            ) : null)
        )}
      </div>
    );
  }
}


export default PostList;
