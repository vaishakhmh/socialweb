import { createMuiTheme }  from '@material-ui/core/styles'

const theme=createMuiTheme({
    palette:{
        primary:{500:'#000000'},
        secondary:{A400:'#b21f66'},
        text:{'primary':'#000000'},
        text:{'secondary':'b21f66'}
    }
})

export default theme;